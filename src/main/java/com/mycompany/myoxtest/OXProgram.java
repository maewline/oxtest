/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.myoxtest;

import java.util.Scanner;

/**
 *
 * @author Arthi
 */
public class OXProgram {

    static char turn = 'X';
    static boolean finish = false;
    static String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
    static Scanner kb = new Scanner(System.in);
    static int row = 0, col = 0;
    static int count;

    public static void main(String[] args) {
        showWelcome();
        while (finish == false) {
            showTable();
            showTurn();
            inputRowCol();
            Process();
            CheckCondition();
        }
        printWinner();
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c] + " ");
            }
            System.out.println(" ");
        }
    }

    public static void showTurn() {
        System.out.println("turn " + turn);
        System.out.print("Please input row, col: ");
    }

    public static void inputRowCol() {
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void Process() {
        try {
            if (table[row - 1][col - 1].equals("-") && turn == 'O') {
                table[row - 1][col - 1] = "O";
                turn = 'X';
                count++;

            } else if (table[row - 1][col - 1].equals("-") && turn == 'X') {
                table[row - 1][col - 1] = "X";
                turn = 'O';
                count++;

            }
        } catch (Exception e) {
            System.out.println();
            System.out.println("Error!! Please input again.");
        }
    }

    public static boolean CheckHorizontal(String[][] table) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0].equals(table[i][1]) && table[i][1].equals(table[i][2]) && !table[i][0].equals("-")) {
                return true;
            }
        }
        return false;
    }

    public static boolean CheckVertical(String[][] table) {
        for (int i = 0; i < 3; i++) {
            if (table[0][i].equals(table[1][i]) && table[1][i].equals(table[2][i]) && !table[0][i].equals("-")) {
                return true;
            }
        }
        return false;
    }

    public static boolean CheckLeftDiagonal(String[][] table) {
        if (table[0][0].equals(table[1][1]) && table[1][1].equals(table[2][2]) && !table[1][1].equals("-")) {
            return true;
        }
        return false;
    }

    public static boolean CheckRightDiagonal(String[][] table) {
        if (table[0][2].equals(table[1][1]) && table[1][1].equals(table[2][0]) && !table[0][2].equals("-")) {
            return true;
        }
        return false;
    }

    public static boolean CheckDraw(int count) {
        if (count == 9) {
            return true;
        }
        return false;
    }

    public static void CheckCondition() {
        if (CheckHorizontal(table)) {
            finish = true;
            showTable();
        } else if (CheckVertical(table)) {
            finish = true;
            showTable();
        } else if (CheckLeftDiagonal(table)) {
            finish = true;
            showTable();
        } else if (CheckRightDiagonal(table)) {
            finish = true;
            showTable();
        } else if (CheckDraw(count)) {
            turn = '-';
            finish = true;
            showTable();
        }

    }

    public static void printWinner() {
        if (turn == 'O') {
            System.out.println(">>>X Win<<<");
        } else if (turn == 'X') {
            System.out.println(">>>O Win<<<");
        } else {
            System.out.println(">>>Draw<<<");
        }
    }

}
