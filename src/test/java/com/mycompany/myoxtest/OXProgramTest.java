/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.myoxtest;

import static com.mycompany.myoxtest.OXProgram.CheckHorizontal;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Arthi
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    
    //testCheckVertical
    @Test
    public void testCheckVerticalPlayerXCol1() {
        String table[][] = {{"X", "-", "-"},
                                {"X", "-", "-"},
                                {"X", "-", "-"}};

        assertEquals(true, OXProgram.CheckVertical(table));
    }
    @Test
    public void testCheckVerticalPlayerXCol2() {
        String table[][] = {{"-", "X", "-"},
                                {"-", "X", "-"},
                                {"-", "O", "-"}};

        assertEquals(false, OXProgram.CheckVertical(table));
    }
    @Test
     public void testCheckVerticalPlayerXCol3() {
        String table[][] = {{"-", "-", "X"},
                                {"-", "-", "X"},
                                {"-", "-", "X"}};

        assertEquals(true, OXProgram.CheckVertical(table));
    }
     
     //testCheckHorizontal
     @Test
     public void testCheckCheckHorizontalPlayerXRow1() {
        String table[][] = {{"X", "X", "X"},
                                {"-", "-", "-"},
                                {"-", "-", "-"}};

        assertEquals(true, OXProgram.CheckHorizontal(table));
    }
     
     @Test
     public void testCheckCheckHorizontalPlayerXRow2() {
        String table[][] = {{"-", "-", "-"},
                                {"X", "X", "O"},
                                {"-", "-", "-"}};

        assertEquals(false, OXProgram.CheckHorizontal(table));
    }
     
     @Test
     public void testCheckCheckHorizontalPlayerXRow3() {
        String table[][] = {{"-", "-", "-"},
                                {"-", "-", "-"},
                                {"X", "X", "X"}};

        assertEquals(true, OXProgram.CheckHorizontal(table));
    }
    
     //testCheckDiagonal
      @Test
     public void testCheckRightDiagonalPlayerX() {
        String table[][] = {{"-", "-", "X"}, 
                                {"-", "X", "-"}, 
                                {"X", "-", "-"}};

        assertEquals(true, OXProgram.CheckRightDiagonal(table));
    } 
     @Test
     public void testCheckLeftDiagonalPlayerX() {
        String table[][] = {{"X", "-", "-"}, 
                                {"-", "X", "-"}, 
                                {"-", "-", "X"}};

        assertEquals(true, OXProgram.CheckLeftDiagonal(table));
    }
     
     //testCheckDraw
      @Test
     public void testCheckDrawPlayerX() {
        int count = 8;
        assertEquals(false, OXProgram.CheckDraw(count));
    }
}
